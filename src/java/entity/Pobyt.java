/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ondra
 */
@Entity
@Table(name = "POBYT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pobyt.findAll", query = "SELECT p FROM Pobyt p"),
    @NamedQuery(name = "Pobyt.findByIdPobytu", query = "SELECT p FROM Pobyt p WHERE p.idPobytu = :idPobytu"),
    @NamedQuery(name = "Pobyt.findByDatumZacatku", query = "SELECT p FROM Pobyt p WHERE p.datumZacatku = :datumZacatku"),
    @NamedQuery(name = "Pobyt.findByDatumKonce", query = "SELECT p FROM Pobyt p WHERE p.datumKonce = :datumKonce"),
    @NamedQuery(name = "Pobyt.findByDatumZacatkuLess", query = "SELECT p FROM Pobyt p WHERE p.datumZacatku BETWEEN :dateToday AND :dateMonth ORDER BY p.cisloPokoje.cisloPokoje"),
    @NamedQuery(name = "Pobyt.findIsRoomFree", query = "SELECT p FROM Pobyt p WHERE ((:dateFrom >= p.datumZacatku AND :dateFrom <= p.datumKonce) OR (:dateTo >= p.datumZacatku AND :dateTo <= p.datumKonce) OR (:dateFrom <= p.datumZacatku AND :dateTo >= p.datumKonce)) AND (:cisloPokoje = p.cisloPokoje.cisloPokoje)"),
    @NamedQuery(name = "Pobyt.findFreeRooms", query = "SELECT p FROM Pobyt p WHERE (:dateFrom >= p.datumZacatku AND :dateFrom <= p.datumKonce) OR (:dateTo >= p.datumZacatku AND :dateTo <= p.datumKonce) OR (:dateFrom <= p.datumZacatku AND :dateTo >= p.datumKonce)"),
    @NamedQuery(name = "Pobyt.findFreeRooms2", query = "SELECT p FROM Pobyt p WHERE (:dateFrom <= p.datumZacatku AND :dateTo <= p.datumZacatku) OR :dateFrom >= p.datumKonce")})

public class Pobyt implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "Pobyt")
    @SequenceGenerator(name="Pobyt", sequenceName = "SEQ_Pobyt", allocationSize = 1) //, allocationSize = 1, initialValue= 1
    @Column(name = "ID_POBYTU")
    private BigDecimal idPobytu;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATUM_ZACATKU")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datumZacatku;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATUM_KONCE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datumKonce;
    @JoinColumn(name = "ID_REZERVACE", referencedColumnName = "ID_REZERVACE")
    @ManyToOne(optional = false)
    private Rezervace idRezervace;
    @JoinColumn(name = "CISLO_POKOJE", referencedColumnName = "CISLO_POKOJE")
    @ManyToOne
    private Pokoj cisloPokoje;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPobytu")
    private Collection<SluzbyVPobytu> sluzbyVPobytuCollection;

    public Pobyt() {
    }

    public Pobyt(BigDecimal idPobytu) {
        this.idPobytu = idPobytu;
    }

    public Pobyt(BigDecimal idPobytu, Date datumZacatku, Date datumKonce) {
        this.idPobytu = idPobytu;
        this.datumZacatku = datumZacatku;
        this.datumKonce = datumKonce;
    }

    public BigDecimal getIdPobytu() {
        return idPobytu;
    }

    public void setIdPobytu(BigDecimal idPobytu) {
        this.idPobytu = idPobytu;
    }

    public Date getDatumZacatku() {
        return datumZacatku;
    }

    public void setDatumZacatku(Date datumZacatku) {
        this.datumZacatku = datumZacatku;
    }

    public Date getDatumKonce() {
        return datumKonce;
    }

    public void setDatumKonce(Date datumKonce) {
        this.datumKonce = datumKonce;
    }

    public Rezervace getIdRezervace() {
        return idRezervace;
    }

    public void setIdRezervace(Rezervace idRezervace) {
        this.idRezervace = idRezervace;
    }

    public Pokoj getCisloPokoje() {
        return cisloPokoje;
    }

    public void setCisloPokoje(Pokoj cisloPokoje) {
        this.cisloPokoje = cisloPokoje;
    }

    @XmlTransient
    public Collection<SluzbyVPobytu> getSluzbyVPobytuCollection() {
        return sluzbyVPobytuCollection;
    }

    public void setSluzbyVPobytuCollection(Collection<SluzbyVPobytu> sluzbyVPobytuCollection) {
        this.sluzbyVPobytuCollection = sluzbyVPobytuCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPobytu != null ? idPobytu.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pobyt)) {
            return false;
        }
        Pobyt other = (Pobyt) object;
        if ((this.idPobytu == null && other.idPobytu != null) || (this.idPobytu != null && !this.idPobytu.equals(other.idPobytu))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Pobyt[ idPobytu=" + idPobytu + " ]";
    }
    
}
