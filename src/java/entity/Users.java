/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ondra
 */
@Entity
@Table(name = "USERS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u"),
    @NamedQuery(name = "Users.findByUserId", query = "SELECT u FROM Users u WHERE u.userId = :userId"),
    @NamedQuery(name = "Users.findByUsername", query = "SELECT u FROM Users u WHERE u.username = :username"),
    @NamedQuery(name = "Users.findByPassword", query = "SELECT u FROM Users u WHERE u.password = :password"),
    @NamedQuery(name = "Users.findByGroupId", query = "SELECT u FROM Users u WHERE u.groupId = :groupId")})
public class Users implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "GROUP_ID")
    private BigInteger groupId;
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    //@NotNull
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "MYNOTIFSEQ")
    @SequenceGenerator(name="MYNOTIFSEQ", sequenceName = "SEQ_Users", allocationSize = 1) //, allocationSize = 1, initialValue= 1
    @Column(name = "USER_ID")
    private BigDecimal userId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "USERNAME")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "PASSWORD")
    private String password;
    @JoinColumn(name = "ZAKAZNIKID", referencedColumnName = "CISLO_DOKLADU")
    @OneToOne
    private Zakaznik zakaznikid;
    @OneToMany(mappedBy = "fkUsers")
    private Collection<Usergroups> usergroupsCollection;

    public Users() {
    }

    public Users(BigDecimal userId) {
        this.userId = userId;
    }

    public Users(BigDecimal userId, String username, String password) {
        this.userId = userId;
        this.username = username;
        this.password = password;
    }

    public BigDecimal getUserId() {
        return userId;
    }

    public void setUserId(BigDecimal userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Zakaznik getZakaznikid() {
        return zakaznikid;
    }

    public void setZakaznikid(Zakaznik zakaznikid) {
        this.zakaznikid = zakaznikid;
    }

    @XmlTransient
    public Collection<Usergroups> getUsergroupsCollection() {
        return usergroupsCollection;
    }

    public void setUsergroupsCollection(Collection<Usergroups> usergroupsCollection) {
        this.usergroupsCollection = usergroupsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Users[ userId=" + userId + " ]";
    }

    public BigInteger getGroupId() {
        return groupId;
    }

    public void setGroupId(BigInteger groupId) {
        this.groupId = groupId;
    }
    
}
