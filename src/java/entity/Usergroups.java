/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ondra
 */
@Entity
@Table(name = "USERGROUPS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usergroups.findAll", query = "SELECT u FROM Usergroups u"),
    @NamedQuery(name = "Usergroups.findByUserId", query = "SELECT u FROM Usergroups u WHERE u.usergroupsPK.userId = :userId"),
    @NamedQuery(name = "Usergroups.findByGroupId", query = "SELECT u FROM Usergroups u WHERE u.usergroupsPK.groupId = :groupId")})
public class Usergroups implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UsergroupsPK usergroupsPK;
    @JoinColumn(name = "FK_USERS", referencedColumnName = "USER_ID")
    @ManyToOne
    private Users fkUsers;
    @JoinColumn(name = "FK_GROUPS", referencedColumnName = "GROUP_ID")
    @ManyToOne
    private Groups fkGroups;

    public Usergroups() {
    }

    public Usergroups(UsergroupsPK usergroupsPK) {
        this.usergroupsPK = usergroupsPK;
    }

    public Usergroups(BigInteger userId, BigInteger groupId) {
        this.usergroupsPK = new UsergroupsPK(userId, groupId);
    }

    public UsergroupsPK getUsergroupsPK() {
        return usergroupsPK;
    }

    public void setUsergroupsPK(UsergroupsPK usergroupsPK) {
        this.usergroupsPK = usergroupsPK;
    }

    public Users getFkUsers() {
        return fkUsers;
    }

    public void setFkUsers(Users fkUsers) {
        this.fkUsers = fkUsers;
    }

    public Groups getFkGroups() {
        return fkGroups;
    }

    public void setFkGroups(Groups fkGroups) {
        this.fkGroups = fkGroups;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usergroupsPK != null ? usergroupsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usergroups)) {
            return false;
        }
        Usergroups other = (Usergroups) object;
        if ((this.usergroupsPK == null && other.usergroupsPK != null) || (this.usergroupsPK != null && !this.usergroupsPK.equals(other.usergroupsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Usergroups[ usergroupsPK=" + usergroupsPK + " ]";
    }
    
}
