/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ondra
 */
@Entity
@Table(name = "SLUZBY_V_POBYTU")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SluzbyVPobytu.findAll", query = "SELECT s FROM SluzbyVPobytu s"),
    @NamedQuery(name = "SluzbyVPobytu.findBySluzbyVPobytuId", query = "SELECT s FROM SluzbyVPobytu s WHERE s.sluzbyVPobytuId = :sluzbyVPobytuId"),
    @NamedQuery(name = "SluzbyVPobytu.findBySjednanaCenaSluzby", query = "SELECT s FROM SluzbyVPobytu s WHERE s.sjednanaCenaSluzby = :sjednanaCenaSluzby")})
public class SluzbyVPobytu implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "Sluzby_v_Pobytu")
    @SequenceGenerator(name="Sluzby_v_Pobytu", sequenceName = "SEQ_Sluzby_v_Pobytu", allocationSize = 1) //, allocationSize = 1, initialValue= 1
    @Column(name = "SLUZBY_V_POBYTU_ID")
    private BigDecimal sluzbyVPobytuId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SJEDNANA_CENA_SLUZBY")
    private BigInteger sjednanaCenaSluzby;
    @JoinColumn(name = "ID_SLUZBY", referencedColumnName = "ID_SLUZBY")
    @ManyToOne(optional = false)
    private Sluzba idSluzby;
    @JoinColumn(name = "ID_POBYTU", referencedColumnName = "ID_POBYTU")
    @ManyToOne(optional = false)
    private Pobyt idPobytu;

    public SluzbyVPobytu() {
    }

    public SluzbyVPobytu(BigDecimal sluzbyVPobytuId) {
        this.sluzbyVPobytuId = sluzbyVPobytuId;
    }

    public SluzbyVPobytu(BigDecimal sluzbyVPobytuId, BigInteger sjednanaCenaSluzby) {
        this.sluzbyVPobytuId = sluzbyVPobytuId;
        this.sjednanaCenaSluzby = sjednanaCenaSluzby;
    }

    public BigDecimal getSluzbyVPobytuId() {
        return sluzbyVPobytuId;
    }

    public void setSluzbyVPobytuId(BigDecimal sluzbyVPobytuId) {
        this.sluzbyVPobytuId = sluzbyVPobytuId;
    }

    public BigInteger getSjednanaCenaSluzby() {
        return sjednanaCenaSluzby;
    }

    public void setSjednanaCenaSluzby(BigInteger sjednanaCenaSluzby) {
        this.sjednanaCenaSluzby = sjednanaCenaSluzby;
    }

    public Sluzba getIdSluzby() {
        return idSluzby;
    }

    public void setIdSluzby(Sluzba idSluzby) {
        this.idSluzby = idSluzby;
    }

    public Pobyt getIdPobytu() {
        return idPobytu;
    }

    public void setIdPobytu(Pobyt idPobytu) {
        this.idPobytu = idPobytu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sluzbyVPobytuId != null ? sluzbyVPobytuId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SluzbyVPobytu)) {
            return false;
        }
        SluzbyVPobytu other = (SluzbyVPobytu) object;
        if ((this.sluzbyVPobytuId == null && other.sluzbyVPobytuId != null) || (this.sluzbyVPobytuId != null && !this.sluzbyVPobytuId.equals(other.sluzbyVPobytuId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.SluzbyVPobytu[ sluzbyVPobytuId=" + sluzbyVPobytuId + " ]";
    }
    
}
