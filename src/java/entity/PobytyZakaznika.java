/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ondra
 */
@Entity
@Table(name = "POBYTY_ZAKAZNIKA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PobytyZakaznika.findAll", query = "SELECT p FROM PobytyZakaznika p"),
    @NamedQuery(name = "PobytyZakaznika.findByCisloDokladu", query = "SELECT p FROM PobytyZakaznika p WHERE p.cisloDokladu = :cisloDokladu"),
    @NamedQuery(name = "PobytyZakaznika.findByIdPobytu", query = "SELECT p FROM PobytyZakaznika p WHERE p.idPobytu = :idPobytu"),
    @NamedQuery(name = "PobytyZakaznika.findByDatumZacatku", query = "SELECT p FROM PobytyZakaznika p WHERE p.datumZacatku = :datumZacatku"),
    @NamedQuery(name = "PobytyZakaznika.findByDatumKonce", query = "SELECT p FROM PobytyZakaznika p WHERE p.datumKonce = :datumKonce"),
    @NamedQuery(name = "PobytyZakaznika.findByIdRezervace", query = "SELECT p FROM PobytyZakaznika p WHERE p.idRezervace = :idRezervace")})
public class PobytyZakaznika implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CISLO_DOKLADU")
    private BigInteger cisloDokladu;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_POBYTU")
    private BigInteger idPobytu;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATUM_ZACATKU")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datumZacatku;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATUM_KONCE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datumKonce;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_REZERVACE")
    private BigInteger idRezervace;
    @JoinColumn(name = "CISLO_DOKLADU", referencedColumnName = "CISLO_DOKLADU", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Zakaznik zakaznik;
    @JoinColumn(name = "ID_REZERVACE", referencedColumnName = "ID_REZERVACE", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Rezervace rezervace;
    @JoinColumn(name = "CISLO_POKOJE", referencedColumnName = "CISLO_POKOJE")
    @ManyToOne
    private Pokoj cisloPokoje;

    public PobytyZakaznika() {
    }

    public PobytyZakaznika(BigInteger idRezervace) {
        this.idRezervace = idRezervace;
    }

    public PobytyZakaznika(BigInteger idRezervace, Date datumZacatku, Date datumKonce) {
        this.idRezervace = idRezervace;
        this.datumZacatku = datumZacatku;
        this.datumKonce = datumKonce;
    }

    public BigInteger getCisloDokladu() {
        return cisloDokladu;
    }

    public void setCisloDokladu(BigInteger cisloDokladu) {
        this.cisloDokladu = cisloDokladu;
    }

    public BigInteger getIdPobytu() {
        return idPobytu;
    }

    public void setIdPobytu(BigInteger idPobytu) {
        this.idPobytu = idPobytu;
    }

    public Date getDatumZacatku() {
        return datumZacatku;
    }

    public void setDatumZacatku(Date datumZacatku) {
        this.datumZacatku = datumZacatku;
    }

    public Date getDatumKonce() {
        return datumKonce;
    }

    public void setDatumKonce(Date datumKonce) {
        this.datumKonce = datumKonce;
    }

    public BigInteger getIdRezervace() {
        return idRezervace;
    }

    public void setIdRezervace(BigInteger idRezervace) {
        this.idRezervace = idRezervace;
    }

    public Zakaznik getZakaznik() {
        return zakaznik;
    }

    public void setZakaznik(Zakaznik zakaznik) {
        this.zakaznik = zakaznik;
    }

    public Rezervace getRezervace() {
        return rezervace;
    }

    public void setRezervace(Rezervace rezervace) {
        this.rezervace = rezervace;
    }

    public Pokoj getCisloPokoje() {
        return cisloPokoje;
    }

    public void setCisloPokoje(Pokoj cisloPokoje) {
        this.cisloPokoje = cisloPokoje;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRezervace != null ? idRezervace.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PobytyZakaznika)) {
            return false;
        }
        PobytyZakaznika other = (PobytyZakaznika) object;
        if ((this.idRezervace == null && other.idRezervace != null) || (this.idRezervace != null && !this.idRezervace.equals(other.idRezervace))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PobytyZakaznika[ idRezervace=" + idRezervace + " ]";
    }
    
}
