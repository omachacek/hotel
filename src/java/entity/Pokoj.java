/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ondra
 */
@Entity
@Table(name = "POKOJ")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pokoj.findAll", query = "SELECT p FROM Pokoj p"),
    @NamedQuery(name = "Pokoj.findByCisloPokoje", query = "SELECT p FROM Pokoj p WHERE p.cisloPokoje = :cisloPokoje"),
    @NamedQuery(name = "Pokoj.findByPocetLuzek", query = "SELECT p FROM Pokoj p WHERE p.pocetLuzek = :pocetLuzek"),
    @NamedQuery(name = "Pokoj.findByCena", query = "SELECT p FROM Pokoj p WHERE p.cena = :cena"),
    @NamedQuery(name = "Pokoj.findByJeObsazen", query = "SELECT p FROM Pokoj p WHERE p.jeObsazen = :jeObsazen")})
public class Pokoj implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CISLO_POKOJE")
    private BigDecimal cisloPokoje;
    @Basic(optional = false)
    @NotNull
    @Column(name = "POCET_LUZEK")
    private BigInteger pocetLuzek;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CENA")
    private BigInteger cena;
    @Basic(optional = false)
    @NotNull
    @Column(name = "JE_OBSAZEN")
    private BigInteger jeObsazen;
    @OneToMany(mappedBy = "cisloPokoje")
    private Collection<Pobyt> pobytCollection;
    @OneToMany(mappedBy = "cisloPokoje")
    private Collection<PobytyZakaznika> pobytyZakaznikaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cisloPokoje")
    private Collection<SluzbyNaPokoji> sluzbyNaPokojiCollection;

    public Pokoj() {
    }

    public Pokoj(BigDecimal cisloPokoje) {
        this.cisloPokoje = cisloPokoje;
    }

    public Pokoj(BigDecimal cisloPokoje, BigInteger pocetLuzek, BigInteger cena, BigInteger jeObsazen) {
        this.cisloPokoje = cisloPokoje;
        this.pocetLuzek = pocetLuzek;
        this.cena = cena;
        this.jeObsazen = jeObsazen;
    }

    public BigDecimal getCisloPokoje() {
        return cisloPokoje;
    }

    public void setCisloPokoje(BigDecimal cisloPokoje) {
        this.cisloPokoje = cisloPokoje;
    }

    public BigInteger getPocetLuzek() {
        return pocetLuzek;
    }

    public void setPocetLuzek(BigInteger pocetLuzek) {
        this.pocetLuzek = pocetLuzek;
    }

    public BigInteger getCena() {
        return cena;
    }

    public void setCena(BigInteger cena) {
        this.cena = cena;
    }

    public BigInteger getJeObsazen() {
        return jeObsazen;
    }

    public void setJeObsazen(BigInteger jeObsazen) {
        this.jeObsazen = jeObsazen;
    }

    @XmlTransient
    public Collection<Pobyt> getPobytCollection() {
        return pobytCollection;
    }

    public void setPobytCollection(Collection<Pobyt> pobytCollection) {
        this.pobytCollection = pobytCollection;
    }

    @XmlTransient
    public Collection<PobytyZakaznika> getPobytyZakaznikaCollection() {
        return pobytyZakaznikaCollection;
    }

    public void setPobytyZakaznikaCollection(Collection<PobytyZakaznika> pobytyZakaznikaCollection) {
        this.pobytyZakaznikaCollection = pobytyZakaznikaCollection;
    }

    @XmlTransient
    public Collection<SluzbyNaPokoji> getSluzbyNaPokojiCollection() {
        return sluzbyNaPokojiCollection;
    }

    public void setSluzbyNaPokojiCollection(Collection<SluzbyNaPokoji> sluzbyNaPokojiCollection) {
        this.sluzbyNaPokojiCollection = sluzbyNaPokojiCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cisloPokoje != null ? cisloPokoje.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pokoj)) {
            return false;
        }
        Pokoj other = (Pokoj) object;
        if ((this.cisloPokoje == null && other.cisloPokoje != null) || (this.cisloPokoje != null && !this.cisloPokoje.equals(other.cisloPokoje))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Pokoj[ cisloPokoje=" + cisloPokoje + " ]";
    }
    
}
