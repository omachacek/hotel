/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ondra
 */
@Entity
@Table(name = "SLUZBY_NA_POKOJI")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SluzbyNaPokoji.findAll", query = "SELECT s FROM SluzbyNaPokoji s"),
    @NamedQuery(name = "SluzbyNaPokoji.findBySluzbyNaPokojiId", query = "SELECT s FROM SluzbyNaPokoji s WHERE s.sluzbyNaPokojiId = :sluzbyNaPokojiId"),
    @NamedQuery(name = "SluzbyNaPokoji.findByCenaSluzby", query = "SELECT s FROM SluzbyNaPokoji s WHERE s.cenaSluzby = :cenaSluzby")})
public class SluzbyNaPokoji implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "Sluzby_na_Pokoji")
    @SequenceGenerator(name="Sluzby_na_Pokoji", sequenceName = "SEQ_Sluzby_na_Pokoji", allocationSize = 1) //, allocationSize = 1, initialValue= 1
    @Column(name = "SLUZBY_NA_POKOJI_ID")
    private BigDecimal sluzbyNaPokojiId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CENA_SLUZBY")
    private BigInteger cenaSluzby;
    @JoinColumn(name = "ID_SLUZBY", referencedColumnName = "ID_SLUZBY")
    @ManyToOne(optional = false)
    private Sluzba idSluzby;
    @JoinColumn(name = "CISLO_POKOJE", referencedColumnName = "CISLO_POKOJE")
    @ManyToOne(optional = false)
    private Pokoj cisloPokoje;

    public SluzbyNaPokoji() {
    }

    public SluzbyNaPokoji(BigDecimal sluzbyNaPokojiId) {
        this.sluzbyNaPokojiId = sluzbyNaPokojiId;
    }

    public SluzbyNaPokoji(BigDecimal sluzbyNaPokojiId, BigInteger cenaSluzby) {
        this.sluzbyNaPokojiId = sluzbyNaPokojiId;
        this.cenaSluzby = cenaSluzby;
    }

    public BigDecimal getSluzbyNaPokojiId() {
        return sluzbyNaPokojiId;
    }

    public void setSluzbyNaPokojiId(BigDecimal sluzbyNaPokojiId) {
        this.sluzbyNaPokojiId = sluzbyNaPokojiId;
    }

    public BigInteger getCenaSluzby() {
        return cenaSluzby;
    }

    public void setCenaSluzby(BigInteger cenaSluzby) {
        this.cenaSluzby = cenaSluzby;
    }

    public Sluzba getIdSluzby() {
        return idSluzby;
    }

    public void setIdSluzby(Sluzba idSluzby) {
        this.idSluzby = idSluzby;
    }

    public Pokoj getCisloPokoje() {
        return cisloPokoje;
    }

    public void setCisloPokoje(Pokoj cisloPokoje) {
        this.cisloPokoje = cisloPokoje;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sluzbyNaPokojiId != null ? sluzbyNaPokojiId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SluzbyNaPokoji)) {
            return false;
        }
        SluzbyNaPokoji other = (SluzbyNaPokoji) object;
        if ((this.sluzbyNaPokojiId == null && other.sluzbyNaPokojiId != null) || (this.sluzbyNaPokojiId != null && !this.sluzbyNaPokojiId.equals(other.sluzbyNaPokojiId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.SluzbyNaPokoji[ sluzbyNaPokojiId=" + sluzbyNaPokojiId + " ]";
    }
    
}
