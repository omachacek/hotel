/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ondra
 */
@Entity
@Table(name = "REZERVACE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rezervace.findAll", query = "SELECT r FROM Rezervace r"),
    @NamedQuery(name = "Rezervace.findByIdRezervace", query = "SELECT r FROM Rezervace r WHERE r.idRezervace = :idRezervace"),
    @NamedQuery(name = "Rezervace.findByDatumVytvoreni", query = "SELECT r FROM Rezervace r WHERE r.datumVytvoreni = :datumVytvoreni"),
    @NamedQuery(name = "Rezervace.findByDatumPotvrzeni", query = "SELECT r FROM Rezervace r WHERE r.datumPotvrzeni = :datumPotvrzeni"),
    @NamedQuery(name = "Rezervace.findByDatumZaplaceni", query = "SELECT r FROM Rezervace r WHERE r.datumZaplaceni = :datumZaplaceni"),
    @NamedQuery(name = "Rezervace.findByJeVHistorii", query = "SELECT r FROM Rezervace r WHERE r.jeVHistorii = :jeVHistorii")})
public class Rezervace implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "Rezervace")
    @SequenceGenerator(name="Rezervace", sequenceName = "SEQ_Rezervace", allocationSize = 1) //, allocationSize = 1, initialValue= 1
    @Column(name = "ID_REZERVACE")
    private BigDecimal idRezervace;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATUM_VYTVORENI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datumVytvoreni;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATUM_POTVRZENI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datumPotvrzeni;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATUM_ZAPLACENI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datumZaplaceni;
    @Basic(optional = false)
    @NotNull
    @Column(name = "JE_V_HISTORII")
    private BigInteger jeVHistorii;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRezervace")
    private Collection<Pobyt> pobytCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rezervace")
    private Collection<PobytyZakaznika> pobytyZakaznikaCollection;
    @JoinColumn(name = "CISLO_DOKLADU", referencedColumnName = "CISLO_DOKLADU")
    @ManyToOne(optional = false)
    private Zakaznik cisloDokladu;

    public Rezervace() {
    }

    public Rezervace(BigDecimal idRezervace) {
        this.idRezervace = idRezervace;
    }

    public Rezervace(BigDecimal idRezervace, Date datumVytvoreni, Date datumPotvrzeni, Date datumZaplaceni, BigInteger jeVHistorii) {
        this.idRezervace = idRezervace;
        this.datumVytvoreni = datumVytvoreni;
        this.datumPotvrzeni = datumPotvrzeni;
        this.datumZaplaceni = datumZaplaceni;
        this.jeVHistorii = jeVHistorii;
    }

    public BigDecimal getIdRezervace() {
        return idRezervace;
    }

    public void setIdRezervace(BigDecimal idRezervace) {
        this.idRezervace = idRezervace;
    }

    public Date getDatumVytvoreni() {
        return datumVytvoreni;
    }

    public void setDatumVytvoreni(Date datumVytvoreni) {
        this.datumVytvoreni = datumVytvoreni;
    }

    public Date getDatumPotvrzeni() {
        return datumPotvrzeni;
    }

    public void setDatumPotvrzeni(Date datumPotvrzeni) {
        this.datumPotvrzeni = datumPotvrzeni;
    }

    public Date getDatumZaplaceni() {
        return datumZaplaceni;
    }

    public void setDatumZaplaceni(Date datumZaplaceni) {
        this.datumZaplaceni = datumZaplaceni;
    }

    public BigInteger getJeVHistorii() {
        return jeVHistorii;
    }

    public void setJeVHistorii(BigInteger jeVHistorii) {
        this.jeVHistorii = jeVHistorii;
    }

    @XmlTransient
    public Collection<Pobyt> getPobytCollection() {
        return pobytCollection;
    }

    public void setPobytCollection(Collection<Pobyt> pobytCollection) {
        this.pobytCollection = pobytCollection;
    }

    @XmlTransient
    public Collection<PobytyZakaznika> getPobytyZakaznikaCollection() {
        return pobytyZakaznikaCollection;
    }

    public void setPobytyZakaznikaCollection(Collection<PobytyZakaznika> pobytyZakaznikaCollection) {
        this.pobytyZakaznikaCollection = pobytyZakaznikaCollection;
    }

    public Zakaznik getCisloDokladu() {
        return cisloDokladu;
    }

    public void setCisloDokladu(Zakaznik cisloDokladu) {
        this.cisloDokladu = cisloDokladu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRezervace != null ? idRezervace.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rezervace)) {
            return false;
        }
        Rezervace other = (Rezervace) object;
        if ((this.idRezervace == null && other.idRezervace != null) || (this.idRezervace != null && !this.idRezervace.equals(other.idRezervace))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Rezervace[ idRezervace=" + idRezervace + " ]";
    }
    
    public String stringOfDatumPotvrzeni() {
        if(datumPotvrzeni.compareTo(new Date(Calendar.ERA)) == 0) {
            return "Nepotvrzeno";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM. yyyy");
        return sdf.format( datumPotvrzeni);
        //return datumPotvrzeni.toString();
    }
    
    public String stringOfDatumZaplaceni() {
        if(datumZaplaceni.compareTo(new Date(Calendar.ERA)) == 0) {
            return "Nezaplaceno";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM. yyyy");
        return sdf.format( datumZaplaceni);
        //return datumZaplaceni.toString();
    }
    
}
