/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ondra
 */
@Entity
@Table(name = "ZAKAZNIK")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Zakaznik.findAll", query = "SELECT z FROM Zakaznik z"),
    @NamedQuery(name = "Zakaznik.findByCisloDokladu", query = "SELECT z FROM Zakaznik z WHERE z.cisloDokladu = :cisloDokladu"),
    @NamedQuery(name = "Zakaznik.findByJmeno", query = "SELECT z FROM Zakaznik z WHERE z.jmeno = :jmeno"),
    @NamedQuery(name = "Zakaznik.findByPrijmeni", query = "SELECT z FROM Zakaznik z WHERE z.prijmeni = :prijmeni"),
    @NamedQuery(name = "Zakaznik.findByDatumNarozeni", query = "SELECT z FROM Zakaznik z WHERE z.datumNarozeni = :datumNarozeni"),
    @NamedQuery(name = "Zakaznik.findByAdresa", query = "SELECT z FROM Zakaznik z WHERE z.adresa = :adresa")})
public class Zakaznik implements Serializable {
    
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CISLO_DOKLADU")
    private BigDecimal cisloDokladu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "JMENO")
    private String jmeno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "PRIJMENI")
    private String prijmeni;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATUM_NAROZENI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datumNarozeni;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "ADRESA")
    private String adresa;
    @OneToOne(mappedBy = "zakaznikid")
    private Users usersCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "zakaznik")
    private Collection<PobytyZakaznika> pobytyZakaznikaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cisloDokladu")
    private Collection<Rezervace> rezervaceCollection;

    public Zakaznik() {
    }

    public Zakaznik(BigDecimal cisloDokladu) {
        this.cisloDokladu = cisloDokladu;
    }

    public Zakaznik(BigDecimal cisloDokladu, BigInteger jeOp, String jmeno, String prijmeni, Date datumNarozeni, String adresa) {
        this.cisloDokladu = cisloDokladu;
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
        this.datumNarozeni = datumNarozeni;
        this.adresa = adresa;
    }

    public BigDecimal getCisloDokladu() {
        return cisloDokladu;
    }

    public void setCisloDokladu(BigDecimal cisloDokladu) {
        this.cisloDokladu = cisloDokladu;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public String getPrijmeni() {
        return prijmeni;
    }

    public void setPrijmeni(String prijmeni) {
        this.prijmeni = prijmeni;
    }

    public Date getDatumNarozeni() {
        return datumNarozeni;
    }

    public void setDatumNarozeni(Date datumNarozeni) {
        this.datumNarozeni = datumNarozeni;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    @XmlTransient
    public Users getUsersCollection() {
        return usersCollection;
    }

    public void setUsersCollection(Users usersCollection) {
        this.usersCollection = usersCollection;
    }

    @XmlTransient
    public Collection<PobytyZakaznika> getPobytyZakaznikaCollection() {
        return pobytyZakaznikaCollection;
    }

    public void setPobytyZakaznikaCollection(Collection<PobytyZakaznika> pobytyZakaznikaCollection) {
        this.pobytyZakaznikaCollection = pobytyZakaznikaCollection;
    }

    @XmlTransient
    public Collection<Rezervace> getRezervaceCollection() {
        return rezervaceCollection;
    }

    public void setRezervaceCollection(Collection<Rezervace> rezervaceCollection) {
        this.rezervaceCollection = rezervaceCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cisloDokladu != null ? cisloDokladu.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Zakaznik)) {
            return false;
        }
        Zakaznik other = (Zakaznik) object;
        if ((this.cisloDokladu == null && other.cisloDokladu != null) || (this.cisloDokladu != null && !this.cisloDokladu.equals(other.cisloDokladu))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Zakaznik[ cisloDokladu=" + cisloDokladu + " ]";
    }
}
