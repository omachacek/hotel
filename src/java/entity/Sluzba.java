/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ondra
 */
@Entity
@Table(name = "SLUZBA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sluzba.findAll", query = "SELECT s FROM Sluzba s"),
    @NamedQuery(name = "Sluzba.findByIdSluzby", query = "SELECT s FROM Sluzba s WHERE s.idSluzby = :idSluzby"),
    @NamedQuery(name = "Sluzba.findByNazev", query = "SELECT s FROM Sluzba s WHERE s.nazev = :nazev"),
    @NamedQuery(name = "Sluzba.findByPopis", query = "SELECT s FROM Sluzba s WHERE s.popis = :popis")})
public class Sluzba implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "Sluzba")
    @SequenceGenerator(name="Sluzba", sequenceName = "SEQ_Sluzba", allocationSize = 1) //, allocationSize = 1, initialValue= 1
    @Column(name = "ID_SLUZBY")
    private BigDecimal idSluzby;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "NAZEV")
    private String nazev;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "POPIS")
    private String popis;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSluzby")
    private Collection<SluzbyNaPokoji> sluzbyNaPokojiCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSluzby")
    private Collection<SluzbyVPobytu> sluzbyVPobytuCollection;

    public Sluzba() {
    }

    public Sluzba(BigDecimal idSluzby) {
        this.idSluzby = idSluzby;
    }

    public Sluzba(BigDecimal idSluzby, String nazev, String popis) {
        this.idSluzby = idSluzby;
        this.nazev = nazev;
        this.popis = popis;
    }

    public BigDecimal getIdSluzby() {
        return idSluzby;
    }

    public void setIdSluzby(BigDecimal idSluzby) {
        this.idSluzby = idSluzby;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public String getPopis() {
        return popis;
    }

    public void setPopis(String popis) {
        this.popis = popis;
    }

    @XmlTransient
    public Collection<SluzbyNaPokoji> getSluzbyNaPokojiCollection() {
        return sluzbyNaPokojiCollection;
    }

    public void setSluzbyNaPokojiCollection(Collection<SluzbyNaPokoji> sluzbyNaPokojiCollection) {
        this.sluzbyNaPokojiCollection = sluzbyNaPokojiCollection;
    }

    @XmlTransient
    public Collection<SluzbyVPobytu> getSluzbyVPobytuCollection() {
        return sluzbyVPobytuCollection;
    }

    public void setSluzbyVPobytuCollection(Collection<SluzbyVPobytu> sluzbyVPobytuCollection) {
        this.sluzbyVPobytuCollection = sluzbyVPobytuCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSluzby != null ? idSluzby.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sluzba)) {
            return false;
        }
        Sluzba other = (Sluzba) object;
        if ((this.idSluzby == null && other.idSluzby != null) || (this.idSluzby != null && !this.idSluzby.equals(other.idSluzby))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Sluzba[ idSluzby=" + idSluzby + " ]";
    }
    
}
