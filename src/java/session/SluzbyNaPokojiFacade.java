/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Pokoj;
import entity.Sluzba;
import entity.SluzbyNaPokoji;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author ondra
 */
@Stateless
public class SluzbyNaPokojiFacade extends AbstractFacade<SluzbyNaPokoji> {
    @PersistenceContext(unitName = "HotelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SluzbyNaPokojiFacade() {
        super(SluzbyNaPokoji.class);
    }
 
    public List<SluzbyNaPokoji> getAllByPokoj(String pokoj) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(SluzbyNaPokoji.class));
        List<SluzbyNaPokoji> sluzby = new ArrayList<SluzbyNaPokoji>();
        for(Object o : getEntityManager().createQuery(cq).getResultList()) {
            SluzbyNaPokoji s = (SluzbyNaPokoji) o;
            if(String.valueOf(s.getCisloPokoje().getCisloPokoje()).equals(pokoj)) {
                sluzby.add(s);
            }
        }
        
        return sluzby;
    }
   
    
}
