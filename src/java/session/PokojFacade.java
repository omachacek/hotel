/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Pokoj;
import entity.Users;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author ondra
 */
@Stateless
public class PokojFacade extends AbstractFacade<Pokoj> {
    @PersistenceContext(unitName = "HotelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PokojFacade() {
        super(Pokoj.class);
    }
    
    public Pokoj findByName(String name) {
        Query q = em.createQuery("SELECT p FROM Pokoj p WHERE p.cisloPokoje = " + name);
        try {
            return (Pokoj)q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
    
}
