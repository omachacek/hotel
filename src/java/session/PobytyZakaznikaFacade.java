/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Pobyt;
import entity.PobytyZakaznika;
import entity.Zakaznik;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author ondra
 */
@Stateless
public class PobytyZakaznikaFacade extends AbstractFacade<PobytyZakaznika> {

    @PersistenceContext(unitName = "HotelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PobytyZakaznikaFacade() {
        super(PobytyZakaznika.class);
    }

    
    public List<PobytyZakaznika> findById(BigDecimal findById) {
        TypedQuery<PobytyZakaznika> queryProductsByName = em.createNamedQuery("Zakaznik.findByCisloDokladu", PobytyZakaznika.class);
        queryProductsByName.setParameter("cisloDokladu", findById);
        return (List<PobytyZakaznika>)queryProductsByName.getResultList();
    }
}
