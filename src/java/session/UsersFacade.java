/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Users;
import entity.Zakaznik;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author ondra
 */
@Stateless
public class UsersFacade extends AbstractFacade<Users> {

    @PersistenceContext(unitName = "HotelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsersFacade() {
        super(Users.class);
    }

    public Users findByName(String name) {
        TypedQuery<Users> queryProductsByName = em.createNamedQuery("Users.findByUsername", Users.class);
        queryProductsByName.setParameter("username", name);
        try {
            return queryProductsByName.getSingleResult();
        } catch(NoResultException e) {
            return null;
        }
    }
}
