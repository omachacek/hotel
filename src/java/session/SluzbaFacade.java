/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Sluzba;
import entity.Users;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author ondra
 */
@Stateless
public class SluzbaFacade extends AbstractFacade<Sluzba> {
    @PersistenceContext(unitName = "HotelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SluzbaFacade() {
        super(Sluzba.class);
    }
    
    public Sluzba findByName(String name) {
        TypedQuery<Sluzba> queryProductsByName = em.createNamedQuery("Sluzba.findByNazev", Sluzba.class);
        queryProductsByName.setParameter("nazev", name);
        return queryProductsByName.getSingleResult();
    }
    
}
