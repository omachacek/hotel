/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Pobyt;
import entity.Sluzba;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author ondra
 */
@Stateless
public class PobytFacade extends AbstractFacade<Pobyt> {
    @PersistenceContext(unitName = "HotelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PobytFacade() {
        super(Pobyt.class);
    }
    
    
    public List<Pobyt> getAllFreeRoomsInDates(Date d1, Date d2) {
        TypedQuery<Pobyt> queryProductsByName = em.createNamedQuery("Pobyt.findFreeRooms", Pobyt.class);
        queryProductsByName.setParameter("dateFrom", d1);
        queryProductsByName.setParameter("dateTo", d2);
        return queryProductsByName.getResultList();
    }
    
    public List<Pobyt> getAllFreeRoomsInDates(Date d1, Date d2, int roomNum) {
        TypedQuery<Pobyt> queryProductsByName = em.createNamedQuery("Pobyt.findIsRoomFree", Pobyt.class);
        queryProductsByName.setParameter("dateFrom", d1);
        queryProductsByName.setParameter("dateTo", d2);
        queryProductsByName.setParameter("cisloPokoje", roomNum);
        return queryProductsByName.getResultList();
    }
    
    public List<Pobyt> getAllPobytsInMonth(Date d1, Date d2) {
        Date d = new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 24 * 30)); // 30 dni dopredu
        Date today = new Date(System.currentTimeMillis()); // today
        System.out.println("BETWEEN : " + today.toString() + " AND " + d.toString());
        TypedQuery<Pobyt> queryProductsByName = em.createNamedQuery("Pobyt.findByDatumZacatkuLess", Pobyt.class);
        queryProductsByName.setParameter("dateToday", d1);
        queryProductsByName.setParameter("dateMonth", d2);
        return queryProductsByName.getResultList();
    }
}
