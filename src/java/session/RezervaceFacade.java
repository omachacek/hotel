/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Pokoj;
import entity.Rezervace;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author ondra
 */
@Stateless
public class RezervaceFacade extends AbstractFacade<Rezervace> {
    @PersistenceContext(unitName = "HotelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RezervaceFacade() {
        super(Rezervace.class);
    }
    
    public List<Rezervace> getRezervaceById(String id) {
        Query q = em.createQuery("SELECT r FROM Rezervace r JOIN r.cisloDokladu z WHERE z.cislo_dokladu = " + id);
        return (List<Rezervace>)q.getResultList();
    }
    
}
