/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Pokoj;
import entity.Usergroups;
import entity.UsergroupsPK;
import entity.Users;
import entity.Zakaznik;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author ondra
 */
@Stateless
public class ZakaznikFacade extends AbstractFacade<Zakaznik> {
    @PersistenceContext(unitName = "HotelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ZakaznikFacade() {
        super(Zakaznik.class);
    }
    
    public List<Zakaznik> findOnlyUsers() {
        List<Zakaznik> users = new ArrayList<Zakaznik>();
        /*
        // When u have time optimalize it.
        Query q = em.createQuery("SELECT z FROM Zakaznik z");
        
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Usergroups.class));
        List<Usergroups> ll = getEntityManager().createQuery(cq).getResultList();
        
        List<Integer> usersIds = new ArrayList<Integer>();
        for(Usergroups ug : ll) {
            //ug.getFkGroups().getGroupId().intValue();
            //if(ug.getUsergroupsPK().getGroupId().intValue() == 1) {
            //    usersIds.add(ug.getUsergroupsPK().getUserId().intValue());
            //}
        }
        for(Zakaznik z : (List<Zakaznik>)q.getResultList()) {
            if(usersIds.contains(z.getUsersCollection().getUserId().intValue())) {
                users.add(z);
            }
        }
        */
        return users;
    }
    
    public Zakaznik findById(BigDecimal findById) {
        TypedQuery<Zakaznik> queryProductsByName = em.createNamedQuery("Zakaznik.findByCisloDokladu", Zakaznik.class);
        queryProductsByName.setParameter("cisloDokladu", findById);
        try {
            return queryProductsByName.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
    
    public List<Zakaznik> findByGroup(BigDecimal groupId) {
        TypedQuery<Users> queryProductsByName = em.createNamedQuery("Users.findByGroupId", Users.class);
        queryProductsByName.setParameter("groupId", groupId);
        List<Zakaznik> z = new ArrayList<Zakaznik>();
        
        for(Users u : queryProductsByName.getResultList()) {
            z.add(u.getZakaznikid());
        }
        return z;
    }
}
