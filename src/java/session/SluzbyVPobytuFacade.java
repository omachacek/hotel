/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.SluzbyVPobytu;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author ondra
 */
@Stateless
public class SluzbyVPobytuFacade extends AbstractFacade<SluzbyVPobytu> {
    @PersistenceContext(unitName = "HotelPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SluzbyVPobytuFacade() {
        super(SluzbyVPobytu.class);
    }
    
}
