package jsf;

import entity.Pobyt;
import entity.Rezervace;
import entity.Users;
import entity.Zakaznik;
import jsf.util.JsfUtil;
import jsf.util.PaginationHelper;
import session.ZakaznikFacade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import org.icefaces.ace.event.SelectEvent;

@ManagedBean(name = "zakaznikController")
@ViewScoped
public class ZakaznikController implements Serializable {

    
    
    private Zakaznik current;
    private DataModel items = null;
    @EJB
    private session.ZakaznikFacade ejbFacade;
    @EJB
    private session.RezervaceFacade rezervaceFacade;

    @EJB
    private session.UsersFacade usersFacade;
    @EJB
    private session.PobytFacade pobytFacade;
    private Users user;
    
    private boolean isSelected = false;
    //private List<Zakaznik> allZakaznici;
    
    public ZakaznikController() {
    }



    public Zakaznik getSelected() {
        if (current == null) {
            current = new Zakaznik();
        }
        return current;
    }

    private ZakaznikFacade getFacade() {
        return ejbFacade;
    }


    public String prepareView() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        user = usersFacade.findByName(request.getRemoteUser());
        current = user.getZakaznikid();
        return "View";
    }

    public String prepareCreate() {
        current = new Zakaznik();
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ZakaznikCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }
    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ZakaznikUpdated"));
            return "List";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        
        try {
            for(Rezervace r : current.getRezervaceCollection()) {
                for(Pobyt p : r.getPobytCollection()) {
                    pobytFacade.remove(p);
                }
                rezervaceFacade.remove(r);
            }
            usersFacade.remove(current.getUsersCollection());

            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ZakaznikDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
        
        return "List";
    }

    public String prepareDestroy() {
        String result = "/index?faces-redirect=true";
        
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        request.getRemoteUser();
        user = usersFacade.findByName(request.getRemoteUser());
        
        try {
            for(Rezervace r : user.getZakaznikid().getRezervaceCollection()) {
                for(Pobyt p : r.getPobytCollection()) {
                    pobytFacade.remove(p);
                }
                //rezervaceFacade.remove(r);
            }
            getFacade().remove(user.getZakaznikid());
            usersFacade.remove(user);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ZakaznikDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
        
        try {
            request.logout();
        } catch (ServletException e) {
            result = "/loginError?faces-redirect=true";
        }
        return result;
    }
    

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ZakaznikDeleted"));
        } catch (Exception e) {
            JsfUtil.addSuccessMessage(current.toString());
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    public void selectListener(SelectEvent event) {
        isSelected = true;
        current = (Zakaznik) event.getObject();
    }

    public boolean isIsSelected() {
        return isSelected;
    }

    public Zakaznik getCurrent() {
        return current;
    }
    
    
    
}
