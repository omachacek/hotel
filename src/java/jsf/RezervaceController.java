package jsf;

import entity.Pobyt;
import entity.Pokoj;
import entity.Rezervace;
import entity.Sluzba;
import entity.SluzbyNaPokoji;
import entity.SluzbyVPobytu;
import entity.Users;
import entity.Zakaznik;
import jsf.util.JsfUtil;
import session.RezervaceFacade;


import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.GNOME.Bonobo.Stream;
import org.icefaces.ace.event.SelectEvent;

@ManagedBean(name = "rezervaceController")
@ViewScoped
public class RezervaceController implements Serializable {

    
    private static final int MONTH = 30;
    private boolean showFields = true;
    private String zakaznici;
    private Map<String, Integer> zakazniciValue = new LinkedHashMap<String, Integer>();
    private List<Integer> cislaPokoju;
    private List<Integer> allCislaPokoju;
    private Rezervace current;
    @EJB
    private session.SluzbyVPobytuFacade sluzbyVPobytuFacade;
    @EJB
    private session.RezervaceFacade ejbFacade;
    @EJB
    private session.PobytFacade pobytFacade;
    @EJB
    private session.PokojFacade pokojFacade;
    @EJB
    private session.ZakaznikFacade zakaznikFacade;
    @EJB
    private session.UsersFacade usersFacade;
    @EJB
    private session.SluzbyNaPokojiFacade sluzbyNaPokoji;
    @EJB
    private session.SluzbaFacade sluzbaFacade;
    private Integer cisloPokoje;
    private Zakaznik zakaznik;
    private Sluzba sluzba;
    private Users user;
    //private List<Rezervace> allUsers;
    boolean isSelected = false;
    private LinkedHashMap<String, Integer> ourSluzby;
    private int summaryPrice;
    private int priceForNight;
    Calendar calendar = Calendar.getInstance();
    private String[] sluzbySelected;
    private Date maxDateBirth;
    private Date dateToday = new Date();
    private Date dateTommorow = new Date(dateToday.getTime() + (24 * 60 * 60 * 1000));
    private Date dateFrom = new Date(dateTommorow.getTime());
    private Date dateTo = new Date(dateTommorow.getTime());

    public void setSluzbySelected(String[] sluzbySelected) {
        this.sluzbySelected = sluzbySelected;
    }

    public String[] getSluzbySelected() {
        return sluzbySelected;
    }

    public Date getMaxDateBirth() {
        return maxDateBirth;
    }

    public Date getDateToday() {
        return dateToday;
    }

    public Date getDateTommorow() {
        return dateTommorow;
    }

    public void updateDateToday() {
        summaryPrice *= 2;
    }

    public void recalculate() {
        if(dateTo != null && dateFrom != null) {
            summaryPrice = 0;
            for (String k : ourSluzby.keySet()) {
                for (String sl : sluzbySelected) {
                    JsfUtil.addSuccessMessage(k);
                    JsfUtil.addSuccessMessage(sl);
                    
                    //JsfUtil.addSuccessMessage(sl);
                    if (ourSluzby.get(k).toString().equals(sl)) {
                        JsfUtil.addSuccessMessage(k);
                        JsfUtil.addSuccessMessage(k.substring(k.indexOf(" "), k.indexOf("Kč)")));
                        summaryPrice += Integer.valueOf(k.substring(k.indexOf(" "), k.indexOf("Kč)")));
                    }
                }
            }
            
            long diff = dateTo.getTime() - dateFrom.getTime();
            long diffDays = diff / (24 * 60 * 60 * 1000);
            summaryPrice += (int) (priceForNight * diffDays);
        }
    }

    public RezervaceController() {
        calendar.add(Calendar.YEAR, -18);
        maxDateBirth = calendar.getTime();

    }

    
    public void setPriceForNight(int priceForNight) {
        this.priceForNight = priceForNight;
    }

    public int getPriceForNight() {
        return priceForNight;
    }

    public void setSummaryPrice(int summaryPrice) {
        this.summaryPrice = summaryPrice;
    }

    public int getSummaryPrice() {
        return summaryPrice;
    }

    public LinkedHashMap<String, Integer> getOurSluzby() {
        if (ourSluzby == null) {
            ourSluzby = ourSluzby();
        }
        return ourSluzby;
    }

    public void setOurSluzby(LinkedHashMap<String, Integer> ourSluzby) {
        this.ourSluzby = ourSluzby;
    }

    public void updateSluzby() {
        priceForNight = pokojFacade.findByName(String.valueOf(cisloPokoje)).getCena().intValue();
        summaryPrice = priceForNight;
        ourSluzby = ourSluzby();
    }

    private LinkedHashMap<String, Integer> ourSluzby() {
        LinkedHashMap<String, Integer> list = new LinkedHashMap<String, Integer>();
        for (SluzbyNaPokoji s : sluzbyNaPokoji.getAllByPokoj(String.valueOf(cisloPokoje))) {
            list.put(s.getIdSluzby().getNazev() + " (" + s.getCenaSluzby() + "Kč)", s.getIdSluzby().getIdSluzby().intValue());
        }
        return list;
    }

    public Rezervace getCurrent() {
        return current;
    }

    public void setCurrent(Rezervace current) {
        this.current = current;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Users getUser() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        user = usersFacade.findByName(request.getRemoteUser());

        return user;
    }

    public Sluzba getSluzba() {
        if (this.sluzba == null) {
            this.sluzba = new Sluzba();
        }

        return sluzba;
    }

    public void setSluzba(Sluzba sluzba) {
        this.sluzba = sluzba;
    }

    public void setZakaznik(Zakaznik zakaznik) {
        this.zakaznik = zakaznik;
    }

    public Zakaznik getZakaznik() {
        if (this.zakaznik == null) {
            this.zakaznik = new Zakaznik();
        }
        return zakaznik;
    }

    public List<Rezervace> getAllRezervace() {
        return ejbFacade.findAll();
    }

    public void setCisloPokoje(Integer cisloPokoje) {
        ourSluzby = ourSluzby();
        setOurSluzby(ourSluzby);
        this.cisloPokoje = cisloPokoje;
    }

    public Integer getCisloPokoje() {
        return cisloPokoje;
    }

    public boolean copyList(List dest, List src) {
        if (dest == null) {
            dest = new ArrayList();
        } else {
            dest.clear();
        }
        for (Object o : src) {
            dest.add(o);
        }
        return true;
    }

    public List<Integer> getCislaPokoju() {
        if (cislaPokoju == null) {
            List<Integer> pokoje = new ArrayList<Integer>();
            for (Pokoj p : pokojFacade.findAll()) {
                pokoje.add(p.getCisloPokoje().intValue());
            }
            cislaPokoju = pokoje;
            if (cisloPokoje == null) {
                cisloPokoje = pokoje.get(0);
            }
            allCislaPokoju = new ArrayList<Integer>(cislaPokoju.size());
            copyList(allCislaPokoju, cislaPokoju);
        }
        return cislaPokoju;
    }

    public void setDateFrom(Date date) {
        dateFrom = date;
    }

    public void setDateTo(Date date) {
        dateTo = date;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public Rezervace getSelected() {
        if (current == null) {
            current = new Rezervace();
        }
        return current;
    }

    private RezervaceFacade getFacade() {
        return ejbFacade;
    }

    public String prepareCreate() {
        current = new Rezervace();
        return "Create";
    }

    public String create() {
        try {
            if(dateFrom.equals(dateTo)) {
                JsfUtil.addErrorMessage("Nemůžete se registrovat na den bez noci.");
                return "";
            }
            if(dateFrom.after(dateTo)) {
                JsfUtil.addErrorMessage("Datum od je po datumu do.");
                return "";
            }
            if(!pobytFacade.getAllFreeRoomsInDates(dateFrom, dateTo, cisloPokoje).isEmpty()) {
                JsfUtil.addErrorMessage("Pokoj je obsazen v tomto termínu.");
                return "";
            }
            
            
            if (user == null) {
                user = getUser();
            }

            if (dateFrom == null || dateTo == null) {
                return null;
            }

            current = new Rezervace();
            //current.setIdRezervace(new BigDecimal(getFacade().findAll().size() + 1));
            current.setDatumPotvrzeni(new Date(Calendar.ERA));
            current.setDatumVytvoreni(new Date());
            current.setDatumZaplaceni(new Date(Calendar.ERA));

            FacesContext context = FacesContext.getCurrentInstance();
            if (context.getExternalContext().isUserInRole("USER")) {
                current.setCisloDokladu(user.getZakaznikid());
            } else if (context.getExternalContext().isUserInRole("MANAGER") && !showFields) {
                current.setCisloDokladu(zakaznikFacade.findById(new BigDecimal(zakaznici)));
            } else {
                zakaznikFacade.create(zakaznik);
                current.setCisloDokladu(zakaznik);
            }
            current.setJeVHistorii(new BigInteger("0"));
            getFacade().create(current);

            Pobyt p = new Pobyt();
            p.setIdRezervace(current);
            //p.setIdPobytu(new BigDecimal(pobytFacade.findAll().size() + 1));
            p.setDatumKonce(dateTo);
            p.setDatumZacatku(dateFrom);
            p.setCisloPokoje(pokojFacade.findByName(String.valueOf(cisloPokoje)));


            pobytFacade.create(p);

            List<SluzbyVPobytu> ll = new ArrayList<SluzbyVPobytu>();
            for (String k : ourSluzby.keySet()) {

                for (String sl : sluzbySelected) {
                    //JsfUtil.addSuccessMessage(sl);
                    if (ourSluzby.get(k).toString().equals(sl)) {
                        SluzbyVPobytu s = new SluzbyVPobytu();
                        s.setIdPobytu(p);
                        s.setIdSluzby(sluzbaFacade.findByName(k.substring(0, k.indexOf(" "))));
                        s.setSjednanaCenaSluzby(new BigInteger("123"));// FIXME
                        sluzbyVPobytuFacade.create(s);
                        ll.add(s);
                    }
                }
            }
            p.setSluzbyVPobytuCollection(ll);
            pobytFacade.edit(p);

            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("RezervaceCreated"));

            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, e.toString());
            return null;
        }
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("RezervaceUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        performDestroy();
        //updateRezervace();
        return "List";
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("RezervaceDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, e.toString());
        }
    }

    public List<Rezervace> getAllUsers() {
        //updateRezervace();
        //return this.allUsers;
        
        user = getUser();
        List<Rezervace> l = new ArrayList<Rezervace>();
        for (Rezervace r : ejbFacade.findAll()) {
            if (r.getCisloDokladu().getCisloDokladu() == user.getZakaznikid().getCisloDokladu()) {
                l.add(r);
            }
        }
        return l;
        //return (List<Rezervace>)zakaznikFacade.findById(user.getZakaznikid().getCisloDokladu()).getRezervaceCollection();
    }

    /*
     public void updateRezervace() {
        
     user = getUser();
        
     allUsers = (List<Rezervace>)  getFacade().getRezervaceById(user.getZakaznikid().getCisloDokladu().toPlainString());
     }*/
    public void selectListener(SelectEvent event) {
        isSelected = true;
        current = (Rezervace) event.getObject();
    }

    public boolean isRowSelected() {
        return isSelected;
    }

    public String acceptPayback() {
        try {
            current.setDatumZaplaceni(new Date());
            getFacade().edit(current);
            JsfUtil.addSuccessMessage("Datum zaplacení aktualizován");
            //updateRezervace();
            return "List";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String acceptRezervation() {
        try {
            current.setDatumPotvrzeni(new Date());
            getFacade().edit(current);
            JsfUtil.addSuccessMessage("Datum potrvzení aktualizován");
            //updateRezervace();
            return "List";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public void updateDateTommorow() {
        //dateTommorow = (Date)dateFrom.clone();
        dateTommorow = new Date(dateToday.getTime() + (24 * 60 * 60 * 1000 * 5));
    }

    public List<Integer> getZakazniciIds() {
        List<Integer> l = new ArrayList<Integer>();
        for (Zakaznik z : zakaznikFacade.findAll()) {
            l.add(z.getCisloDokladu().intValue());
        }
        return l;
    }

    public Map<String, Integer> getZakazniciValue() {
        Map<String, Integer> l = new LinkedHashMap<String, Integer>();
        l.put("Nový zákazník", -1);
        for (Zakaznik z : zakaznikFacade.findAll()) {
            l.put(String.valueOf(z.getCisloDokladu().intValue()), z.getCisloDokladu().intValue());
        }
        return l;
    }

    public void updateZakaznik() {
        showFields = zakaznici.equals("-1");
    }

    public boolean isShowFields() {
        return showFields;
    }

    public void setShowFields(boolean showFields) {
        this.showFields = showFields;
    }

    public String getZakaznici() {
        return zakaznici;
    }

    public void setZakaznici(String zakaznici) {
        this.zakaznici = zakaznici;
    }

    public void updateView() {
        if (dateTo.before(dateFrom) || dateFrom.equals(dateTo)) {
            JsfUtil.addErrorMessage("Zkontrolujte datumy. Datum 'do' je pred datumem 'od'.");
            return;
        }

        List<Pobyt> p = pobytFacade.getAllFreeRoomsInDates(dateFrom, dateTo);

        cislaPokoju.clear();
        copyList(cislaPokoju, allCislaPokoju);

        List<Integer> ii = new ArrayList<Integer>();
        for (Pobyt q : p) {
            System.out.println("REMOVE : " + q.getCisloPokoje().getCisloPokoje());
            ii.add(q.getCisloPokoje().getCisloPokoje().intValue());
        }
        cislaPokoju.removeAll(ii);

        if (cislaPokoju.isEmpty()) {
            JsfUtil.addErrorMessage("Litujeme, ale v tomto období nejsou žádné pokoje volné.");
            return;
        }
        JsfUtil.addSuccessMessage("Nyní si můžete vybrat z volných pokojů.");
        recalculate();
    }

    public List<String> getHeader() {
        DateFormat formatter = new SimpleDateFormat("dd.MM");

        List<String> headers = new ArrayList<String>();


        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(today);

        headers.add("Pokoj"); // COL1
        headers.add(formatter.format(today)); // COL1
        for (int i = 0; i < MONTH; i++) {
            cal.add(Calendar.DAY_OF_YEAR, 1);
            headers.add(formatter.format(cal.getTime()));
        }

        return headers;
    }

    public List<String[]> getFreeRows() {
        List<Integer> aa = new ArrayList<Integer>();

        Date today = new Date();
        calendar.setTime(today);
        calendar.set(Calendar.DAY_OF_MONTH, 30);

        Date month = calendar.getTime();

        String[] b = new String[MONTH];
        List<String[]> lst = new ArrayList<String[]>();

        List<Pobyt> pobyts = pobytFacade.getAllPobytsInMonth(today, month);

        Date date;

        Set<Integer> rooms = new HashSet<Integer>();

        boolean last;
        boolean jmp;
        for (Pobyt p : pobyts) {
            last = false;
            jmp = false;

            System.out.println("GOING " + p.getCisloPokoje().getCisloPokoje().intValue());
            if (!rooms.contains(p.getCisloPokoje().getCisloPokoje().intValue())) {
                System.out.println("NEW " + p.getCisloPokoje().getCisloPokoje().intValue());
                b = new String[MONTH];
                Arrays.fill(b, "–");
                rooms.add(p.getCisloPokoje().getCisloPokoje().intValue());
                b[0] = String.valueOf(p.getCisloPokoje().getCisloPokoje().intValue());
                aa.add(Integer.valueOf(b[0]));
            } else {
                jmp = true;
            }
            System.out.println("DONE " + p.getCisloPokoje().getCisloPokoje().intValue());
            calendar.setTime(today);
            for (int i = 1; i < MONTH; i++) {
                date = calendar.getTime();
                if ((date.after(p.getDatumZacatku()) && date.before(p.getDatumKonce()))) {
                    System.out.println("Seting to tru");
                    b[i] = "X";
                    last = true;
                }
                if(last) {
                    System.out.println("Setting Another " + date.toString());
                    b[i + 1] = "X";
                    last = false;
                }
                
                calendar.add(Calendar.DAY_OF_YEAR, 1);

            }
            if (jmp) {
                continue;
            }
            lst.add(b);
        }

        // Add Rooms with are not in pobyts, so that are free for all the month
        getCislaPokoju(); // fill in allCislaPokoju
        for (Integer i : allCislaPokoju) {
            if (!aa.contains(i)) {
                b = new String[MONTH];
                Arrays.fill(b, "–");
                b[0] = String.valueOf(i);
                lst.add(b);
            }
        }

        return lst;
    }

    public List<Pobyt> getAllPobytInMonth() {
        System.out.println("From " + dateFrom);
        System.out.println("To: " + dateTo);
        List<Pobyt> p = pobytFacade.getAllPobytsInMonth(dateFrom, dateTo);
        System.out.println("SIZE : " + p.size());
        for (Pobyt pp : p) {
            System.out.println(pp.getIdPobytu());
        }
        return p;
    }
}