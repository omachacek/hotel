/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.security.*;
import entity.Pobyt;
import entity.Usergroups;
import entity.UsergroupsPK;
import entity.Users;
import entity.Zakaznik;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import jsf.util.JsfUtil;

/**
 *
 * @author ondra
 */
@ManagedBean(name = "regBean")
@RequestScoped
public class RegistraceController {

    @EJB
    private session.ZakaznikFacade zakaznikFacade;
    @EJB
    private session.UsersFacade usersFacade;
    @EJB
    private session.UsergroupsFacade usergroupsFacade;
    @EJB
    private session.GroupsFacade groupsFacade;
    private String confirmpassword;
    private Users user;
    private Zakaznik zakaznik;
    private Usergroups usergroups;

    /**
     * Creates a new instance of RegistraceController
     */
    public RegistraceController() {
    }

    public String getUsername() {
        if (user == null) {
            user = new Users();
        }
        return user.getUsername();
    }

    public void setUsername(String username) {
        user.setUsername(username);
    }

    public String getPassword() {
        if (user == null) {
            user = new Users();
        }
        return user.getPassword();
    }

    public void setPassword(String password) {
        user.setPassword(password);
    }

    public String getConfirmpassword() {
        return confirmpassword;
    }

    public void setConfirmpassword(String confirmpassword) {
        this.confirmpassword = confirmpassword;
    }

    public Zakaznik getZakaznik() {
        if (zakaznik == null) {
            zakaznik = new Zakaznik();
        }
        return zakaznik;
    }

    public void setZakaznik(Zakaznik zakaznik) {
        this.zakaznik = zakaznik;
    }

    private void addMessage(FacesMessage message) {
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public String addUser() {
        try {
            if (!confirmpassword.equals(user.getPassword())) {
                JsfUtil.addErrorMessage(new Exception("Hesla se neshodují."),
                        "Hesla se neshodují.");
                return null;
            }
            
            
            if(usersFacade.findByName(user.getUsername()) != null) {
                JsfUtil.addErrorMessage(new Exception("Uživatel se stejným jménem již existuje."),
                        "Uživatel se stejným jménem již existuje.");
                return null;
            }
            
            
            if(zakaznikFacade.findById(zakaznik.getCisloDokladu()) != null) {
                JsfUtil.addErrorMessage(new Exception("Uživatel se stejným dokladem již existuje."),
                        "Uživatel se stejným dokladem již existuje.");
                return null;
            }


            // MD5 hash of password
            byte[] bytesOfMessage = user.getPassword().getBytes("UTF-8");
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] thedigest = md.digest(bytesOfMessage);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < thedigest.length; i++) {
                sb.append(Integer.toString((thedigest[i] & 0xff) + 0x100, 16).substring(1));
            }

            user.setPassword(sb.toString());

            zakaznikFacade.create(zakaznik);
            
            user.setZakaznikid(zakaznik);
            user.setUserId(null);
            
            
            if(FacesContext.getCurrentInstance().getExternalContext().isUserInRole("ADMIN")) {
                user.setGroupId(new BigInteger("2"));
            } else {
                user.setGroupId(new BigInteger("1"));
            }
            
            usersFacade.create(user);
            
            BigInteger i = user.getUserId().toBigInteger();
            
            usergroups = new Usergroups();
            BigInteger gId;
            if(FacesContext.getCurrentInstance().getExternalContext().isUserInRole("ADMIN")) {
                gId = new BigInteger("2");
                
            } else {
                gId = new BigInteger("1");
            }
            usergroups.setUsergroupsPK(new UsergroupsPK(i, gId));
            usergroupsFacade.create(usergroups);

            JsfUtil.addSuccessMessage("Účet úspěšně vytvořen vytvořen.");
            return "/public/admin/login";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, e.toString());
            JsfUtil.addErrorMessage(e, e.getMessage());
            return null;
        }
    }
}
