package jsf;

import entity.Pobyt;
import entity.Pokoj;
import entity.Rezervace;
import entity.Users;
import entity.Zakaznik;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import session.PobytFacade;
import session.PokojFacade;
import session.UsersFacade;
import session.ZakaznikFacade;

/**
 *
 * @author ondra
 */
@ManagedBean(name = "common")
@RequestScoped
public class Common {

    /**
     * Creates a new instance of Common
     */
    public Common() {
    }
    
    public String doLogout() {
        String result = "/index?faces-redirect=true";

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();

        try {
            request.logout();
        } catch (ServletException e) {
            result = "/loginError?faces-redirect=true";
        }
        return result;
    }
    
    public String whatUser() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        String user = request.getRemoteUser();
        return user == null ? "" : "(" + user + ")";
    }
    
    @EJB
    private UsersFacade usersFacade;
    private Zakaznik current;
    public String prepareView() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        Users user = usersFacade.findByName(request.getRemoteUser());
        
        current = user.getZakaznikid();
        return "View";
    }

    public Zakaznik getCurrent() {
        return current;
    }
    
    @EJB
    private ZakaznikFacade zakaznikFacade;
    public List<Zakaznik> getAllZakaznici() {
        //#{facesContext.externalContext.isUserInRole('USER')}
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        if(context.isUserInRole("ADMIN")) {
            return zakaznikFacade.findByGroup(new BigDecimal("2"));
        } else if(context.isUserInRole("MANAGER")) {
            return zakaznikFacade.findByGroup(new BigDecimal("1"));
        } else {
            return new ArrayList<Zakaznik>();
        }
    }
    
    @EJB
    private PokojFacade pokojFacade;
    public List<Integer> getCislaPokoju() {
            List<Integer> pokoje = new ArrayList<Integer>();
            for (Pokoj p : pokojFacade.findAll()) {
                pokoje.add(p.getCisloPokoje().intValue());
            }
            return pokoje;
    }
    
    @EJB
    private PobytFacade pobytFacade;
    
    public List<Pobyt> getAllPobytInMonth() {
        List<Pobyt> p = pobytFacade.getAllPobytsInMonth(null, null);
        System.out.println("SIZE : " + p.size());
        for (Pobyt pp : p ) {
            System.out.println(pp.getIdPobytu());
        }
        return p;
    }
    
    public List<String> getHeader() {
        DateFormat formatter = new SimpleDateFormat("dd.MM");

        List<String> headers = new ArrayList<String>();


        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(today);

        headers.add("Pokoj"); // COL1
        headers.add(formatter.format(today)); // COL1
        for (int i = 0; i < MONTH; i++) {
            cal.add(Calendar.DAY_OF_YEAR, 1);
            headers.add(formatter.format(cal.getTime()));
        }

        return headers;
    }
    
    private static final int MONTH = 30;
    Calendar calendar = Calendar.getInstance();
    

    public List<String[]> getFreeRows() {
        List<Integer> aa = new ArrayList<Integer>();

        Date today = new Date();
        calendar.setTime(today);
        calendar.set(Calendar.DAY_OF_MONTH, 30);

        Date month = calendar.getTime();

        String[] b = new String[MONTH];
        List<String[]> lst = new ArrayList<String[]>();

        List<Pobyt> pobyts = pobytFacade.getAllPobytsInMonth(today, month);

        Date date;

        Set<Integer> rooms = new HashSet<Integer>();

        boolean jmp;
        boolean last;
        for (Pobyt p : pobyts) {
            jmp = false;
            last = false;

            System.out.println("GOING " + p.getCisloPokoje().getCisloPokoje().intValue());
            if (!rooms.contains(p.getCisloPokoje().getCisloPokoje().intValue())) {
                System.out.println("NEW " + p.getCisloPokoje().getCisloPokoje().intValue());
                b = new String[MONTH];
                Arrays.fill(b, "–");
                rooms.add(p.getCisloPokoje().getCisloPokoje().intValue());
                b[0] = String.valueOf(p.getCisloPokoje().getCisloPokoje().intValue());
                aa.add(Integer.valueOf(b[0]));
            } else {
                jmp = true;
            }
            System.out.println("DONE " + p.getCisloPokoje().getCisloPokoje().intValue());
            calendar.setTime(today);
            for (int i = 1; i < MONTH; i++) {
                date = calendar.getTime();
                if ((date.after(p.getDatumZacatku()) && date.before(p.getDatumKonce())) || (date.equals(p.getDatumZacatku()) || date.equals(p.getDatumKonce()))) {
                    b[i] = "X";
                    last = true;
                } else if(last && ((i + 1) < MONTH )) {
                    b[i] = "X";
                    last = false;
                }
                calendar.add(Calendar.DAY_OF_YEAR, 1);

            }
            if (jmp) {
                continue;
            }
            lst.add(b);
        }

        // Add Rooms with are not in pobyts, so that are free for all the month
        getCislaPokoju(); // fill in allCislaPokoju
        for (Integer i : getCislaPokoju()) {
            if (!aa.contains(i)) {
                b = new String[MONTH];
                Arrays.fill(b, "–");
                b[0] = String.valueOf(i);
                lst.add(b);
            }
        }

        return lst;
    }
}
