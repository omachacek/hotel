package jsf;

import entity.Pokoj;
import entity.Sluzba;
import entity.SluzbyNaPokoji;
import jsf.util.JsfUtil;
import jsf.util.PaginationHelper;
import session.SluzbaFacade;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import org.icefaces.ace.event.SelectEvent;


@ManagedBean(name="sluzbaController")
@SessionScoped
public class SluzbaController implements Serializable {


    private Sluzba current;
    private Sluzba createSluzba;
    
    @EJB private session.SluzbaFacade ejbFacade;
    
    @EJB
    private session.PokojFacade pokojFacade;
    private String []pokojeSelected;
    private LinkedHashMap<String, Integer> ourPokoje;
    private int cena;
    private boolean isSelected = false;

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public void setOurPokoje(LinkedHashMap<String, Integer> ourPokoje) {
        this.ourPokoje = ourPokoje;
    }

    public LinkedHashMap<String, Integer> getOurPokoje() {
        LinkedHashMap<String, Integer> m = new LinkedHashMap<String, Integer>();
        for(Pokoj p : pokojFacade.findAll()) {
            
        }
        return m;
    }
    
    

    

    public void setCurrent(Sluzba current) {
        this.current = current;
    }

    
    

    public String[] getPokojeSelected() {
        return pokojeSelected;
    }

    public void setPokojeSelected(String[] pokojeSelected) {
        this.pokojeSelected = pokojeSelected;
    }
    
    

    public SluzbaController() {
    }

    public List<Sluzba> getAllSluzba() {
        return ejbFacade.findAll();
    }
    

    private SluzbaFacade getFacade() {
        return ejbFacade;
    }


    public String prepareCreate() {
        current = new Sluzba();
        return "Create";
    }

    public String create() {
        try {
            /*
            List<SluzbyVPobytu> ll =new ArrayList<SluzbyVPobytu>();
            for(String k : ourSluzby.keySet()) {
                
                for(String sl : sluzbySelected) {
                    JsfUtil.addSuccessMessage(sl);
                    if (ourSluzby.get(k).toString().equals(sl)) {
                        SluzbyVPobytu s = new SluzbyVPobytu();
                        s.setIdPobytu(p);
                        s.setIdSluzby(sluzbaFacade.findByName(k.substring(0, k.indexOf(" "))));
                        s.setSjednanaCenaSluzby(new BigInteger("123"));// FIXME
                        sluzbyVPobytuFacade.create(s);
                        ll.add(s);
                    }
                }
            }
            p.setSluzbyVPobytuCollection(ll);
            
            for(String p : pokojeSelected) {
                SluzbyNaPokoji s = new SluzbyNaPokoji();
                s.setCenaSluzby(new BigInteger(String.valueOf(cena)));
                s.setCisloPokoje(null);
            }
            current.set
            getFacade().create(current);
            */
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("SluzbaCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("SluzbaUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        //current = (Sluzba)getItems().getRowData();
        //selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        //recreatePagination();
        //recreateModel();
        return "List";
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("SluzbaDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }


    public Sluzba getCurrent() {
        return current;
    }

    public void selectListener(SelectEvent event) {
        isSelected = true;
        current = (Sluzba) event.getObject();
    }

    public boolean isIsSelected() {
        return isSelected;
    }

    public Sluzba getCreateSluzba() {
        return createSluzba;
    }

    public void setCreateSluzba(Sluzba createSluzba) {
        this.createSluzba = createSluzba;
    }

    
}
