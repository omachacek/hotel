package jsf;

import entity.Pobyt;
import entity.Rezervace;
import entity.SluzbyVPobytu;
import entity.Users;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.servlet.http.HttpServletRequest;
import jsf.util.JsfUtil;
import jsf.util.PaginationHelper;
import org.icefaces.ace.event.SelectEvent;
import session.PobytFacade;

@ManagedBean(name = "pobytController")
@ViewScoped
public class PobytController implements Serializable {

    private Pobyt current;
    
    @EJB
    private session.PobytFacade ejbFacade;
    @EJB
    private session.PokojFacade pokojFacade;
    @EJB
    private session.RezervaceFacade rezervaceFacade;
    @EJB
    private session.UsersFacade usersFacade;
    @EJB
    private session.ZakaznikFacade zakaznikFacade;
    @EJB
    private session.PobytyZakaznikaFacade pobytyZakaznikaFacade;
    private Users user;
    
    private boolean isSelected;
    private List<Pobyt> userPobyts;
    private List<Pobyt> allPobyts;

    public PobytController() {
    }

    public Users getUser() {
        
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
            user = usersFacade.findByName(request.getRemoteUser());
        
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    private PobytFacade getFacade() {
        return ejbFacade;
    }

    public String prepareCreate() {
        current = new Pobyt();
        return "Create";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("PobytUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage("Pobyt smazán.");
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, e.toString());
        }

        getAllPobyts();
        getAllUserPobyts();
        return "List";
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("PobytDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    public List<Pobyt> getAllPobyts() {
        return (allPobyts = getFacade().findAll());
    }

    public List<Pobyt> getAllUserPobyts() {
        /*
        user = getUser();
        List<Pobyt> p = new ArrayList<Pobyt>();
        for (Rezervace r : zakaznikFacade.findById(user.getZakaznikid().getCisloDokladu()).getRezervaceCollection()) {
            for (Pobyt t : r.getPobytCollection()) {
                p.add(t);
            }
        }
        userPobyts = p;
        return p;
        */
        user = getUser();
        List<Pobyt> l = new ArrayList<Pobyt>();
        for(Pobyt p :ejbFacade.findAll()) {
            if(p.getIdRezervace().getCisloDokladu().getCisloDokladu() == user.getZakaznikid().getCisloDokladu()) {
                l.add(p);
            }
        }
        return l;
        //return pobytyZakaznikaFacade.findById(user.getZakaznikid().getCisloDokladu());
    }

    
    public String getSluzby() {
        String s = "";
        if(current == null) return "null";
        for(SluzbyVPobytu svp : current.getSluzbyVPobytuCollection()) {
            //for(SluzbyVPobytu svp : p.getSluzbyVPobytuCollection()) {
            s+="...";
                s += svp.getIdSluzby().getNazev();
            //}
        }
        return s;
    }
    
    public void selectListener(SelectEvent event) {
        isSelected = true;
        current = (Pobyt) event.getObject();
    }

    public boolean isIsSelected() {
        return isSelected;
    }

    public Pobyt getCurrent() {
        return current;
    }
    
    
}
