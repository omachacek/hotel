connect test_user/test_user@XE

create user test_user
identified by test_user;
grant all privileges to test_user;

CREATE TABLE groups ( 
  group_id INT NOT NULL,
  group_name varchar(20) NOT NULL,
  group_desc varchar(200) DEFAULT NULL,
  PRIMARY KEY (group_id)
);

CREATE TABLE users(
  user_id INT NOT NULL,
  username varchar(10) NOT NULL,
  password varchar(32) NOT NULL,
  zakaznikid REFERENCES Zakaznik(CISLO_DOKLADU),
  PRIMARY KEY(user_id)
);

CREATE TABLE usergroups(
  user_id INT NOT NULL,
  group_id INT NOT NULL,
  PRIMARY KEY (user_id,group_id),
  fk_groups REFERENCES groups(group_id),
  fk_users REFERENCES users(user_id)
);
 
CREATE VIEW vuserrole AS
 
SELECT  u.username, u.password, g.group_name
 FROM usergroups ug
 INNER JOIN users u ON u.user_id = ug.user_id
 INNER JOIN groups g ON g.group_id =  ug.group_id; 
 
INSERT INTO groups(group_id,group_name,group_desc) VALUES
  (1,'USER','Regular users'),
  (2,'MANAGER','Manage all'),
  (3,'ADMIN','Admin managers');
 
-- do not work properly
INSERT  INTO users(user_id,username,password, zakaznikid) VALUES
  (1,'user', 'ee11cbb19052e40b07aac0ca060c23ee', 12345678),
  (2,'hotel', 'e919c49d5f0cd737285367810a3394d0', 12345679),
  (3,'admin', '21232f297a57a5a743894a0e4a801fc3', 52345678);
   
INSERT INTO usergroups(user_id,group_id) VALUES (1,1),(2,2),(3,3);


