DROP TABLE Sluzba CASCADE CONSTRAINTS;
DROP TABLE Pokoj CASCADE CONSTRAINTS;
DROP TABLE Sluzby_na_Pokoji CASCADE CONSTRAINTS;
DROP TABLE Pobyt CASCADE CONSTRAINTS;
DROP TABLE Sluzby_v_Pobytu CASCADE CONSTRAINTS;
DROP TABLE Rezervace CASCADE CONSTRAINTS;
DROP TABLE Zakaznik CASCADE CONSTRAINTS;
DROP TABLE Users CASCADE CONSTRAINTS;
DROP TABLE groups CASCADE CONSTRAINTS;
DROP TABLE usergroups CASCADE CONSTRAINTS;

DROP VIEW pobyty_zakaznika;
DROP VIEW vuserrole;
DROP SEQUENCE SEQ_Pobyt;
DROP SEQUENCE SEQ_Users;
DROP SEQUENCE SEQ_Rezervace;
DROP SEQUENCE SEQ_Sluzby_na_Pokoji;
DROP SEQUENCE SEQ_Sluzby_v_Pobytu;
DROP SEQUENCE SEQ_Sluzba;

CREATE TABLE Sluzba(
	ID_Sluzby INT PRIMARY KEY,
	Nazev VARCHAR2(30) NOT NULL,
	Popis VARCHAR2(150) NOT NULL
);
CREATE TABLE Pokoj(
	Cislo_Pokoje INT PRIMARY KEY,
	Pocet_Luzek INT NOT NULL,
	Cena INT NOT NULL,
	je_Obsazen INT NOT NULL 
);
CREATE TABLE Sluzby_na_Pokoji(
  Sluzby_na_Pokoji_ID INT PRIMARY KEY,
	Cislo_Pokoje REFERENCES Pokoj(Cislo_Pokoje) NOT NULL,
	ID_Sluzby REFERENCES Sluzba(ID_Sluzby) NOT NULL,
	Cena_Sluzby INT NOT NULL
);
CREATE TABLE Zakaznik(
	Cislo_Dokladu INT PRIMARY KEY,
	Jmeno VARCHAR2(30) NOT NULL,
	Prijmeni VARCHAR2(30) NOT NULL,
	Datum_Narozeni DATE NOT NULL,
	Adresa VARCHAR2(150) NOT NULL
);
CREATE TABLE Rezervace(
	ID_Rezervace INT PRIMARY KEY,
	Datum_Vytvoreni DATE NOT NULL,
	Datum_Potvrzeni DATE NOT NULL,
	Datum_Zaplaceni DATE NOT NULL,
	je_v_Historii INT NOT NULL,
	cislo_Dokladu REFERENCES ZAKAZNIK(CISLO_DOKLADU) NOT NULL
);
CREATE TABLE Pobyt(
	ID_Pobytu INT PRIMARY KEY,
	Datum_Zacatku DATE NOT NULL,
	Datum_Konce DATE NOT NULL,
	Cislo_Pokoje REFERENCES Pokoj(Cislo_Pokoje),
	ID_Rezervace REFERENCES Rezervace(ID_Rezervace) ON DELETE CASCADE NOT NULL
);
CREATE TABLE Sluzby_v_Pobytu(
	Sluzby_v_Pobytu_ID INT PRIMARY KEY,
	ID_Pobytu REFERENCES Pobyt(ID_Pobytu) ON DELETE CASCADE NOT NULL,
	ID_Sluzby REFERENCES Sluzba(ID_Sluzby) NOT NULL,
	Sjednana_cena_sluzby INT NOT NULL
);

CREATE TABLE groups ( 
  group_id INT NOT NULL,
  group_name varchar(20) NOT NULL,
  group_desc varchar(200) DEFAULT NULL,
  PRIMARY KEY (group_id)
);

CREATE TABLE users(
  user_id INT NOT NULL,
  username varchar(10) NOT NULL,
  password varchar(32) NOT NULL,
  zakaznikid REFERENCES Zakaznik(CISLO_DOKLADU) ON DELETE CASCADE,
  group_id INT NOT NULL,
  PRIMARY KEY(user_id)
);

CREATE TABLE usergroups(
  user_id INT NOT NULL,
  group_id INT NOT NULL,
  PRIMARY KEY (user_id,group_id),
  fk_groups REFERENCES groups(group_id),
  fk_users REFERENCES users(user_id)
);

CREATE SEQUENCE SEQ_Pobyt
 INCREMENT BY 1
 START WITH 4
 NOMAXVALUE
 MINVALUE 0;
 
 CREATE SEQUENCE SEQ_Rezervace
 INCREMENT BY 1
 START WITH 3
 NOMAXVALUE
 MINVALUE 0;
 
 CREATE SEQUENCE SEQ_Sluzby_na_Pokoji
 INCREMENT BY 1
 START WITH 5
 NOMAXVALUE
 MINVALUE 0;
 
 CREATE SEQUENCE SEQ_Sluzby_v_Pobytu
 INCREMENT BY 1
 START WITH 4
 NOMAXVALUE
 MINVALUE 0;
 
 CREATE SEQUENCE SEQ_Users
 INCREMENT BY 1
 START WITH 4
 NOMAXVALUE
 MINVALUE 0;
 
 CREATE SEQUENCE SEQ_Sluzba
 INCREMENT BY 1
 START WITH 3
 NOMAXVALUE
 MINVALUE 0;

CREATE VIEW vuserrole AS
 
SELECT  u.username, u.password, g.group_name
 FROM usergroups ug
 INNER JOIN users u ON u.user_id = ug.user_id
 INNER JOIN groups g ON g.group_id =  ug.group_id; 
 

CREATE VIEW pobyty_zakaznika AS
	SELECT CISLO_DOKLADU, ID_Pobytu, Datum_zacatku, Datum_konce, Cislo_pokoje, ID_rezervace
	FROM zakaznik natural join rezervace natural join pobyt
WITH CHECK OPTION;

--CREATE OR REPLACE PROCEDURE pridej_zakaznika 
--  (cdin IN INT,jein IN INT,jmin IN VARCHAR2,prin IN VARCHAR2,dain IN DATE,adin IN VARCHAR2)
--IS
--  BEGIN
    --INSERT INTO Zakaznik (Cislo_Dokladu,je_OP,Jmeno,Prijmeni,Datum_Narozeni,Adresa)
    --VALUES(cdin,jein,jmin,prin,dain,adin);
  --EXCEPTION
--    WHEN DUP_VAL_ON_INDEX THEN
      --DBMS_OUTPUT.PUT_LINE('Zadano duplicitni cislo dokladu');
      --DBMS_OUTPUT.PUT_LINE('Zadano duplicitni cislo dokladu');
    --WHEN OTHERS THEN
--      DBMS_OUTPUT.PUT_LINE('Neznama chyba');
      --DBMS_OUTPUT.PUT_LINE('Neznama chyba');
  --END;
--/


INSERT INTO Sluzba VALUES (1,'Sauna','Volny vstup do sauny.');
INSERT INTO Sluzba VALUES (2,'Wifi','Pristup na internet pres lokalni Wifi sit.');

INSERT INTO Pokoj VALUES (235, 2, 1600, 1);
INSERT INTO Pokoj VALUES (101, 3, 1700, 0);
INSERT INTO Pokoj VALUES (102, 3, 1750, 0);

INSERT INTO Sluzby_na_Pokoji VALUES (1, 235, 1, 100);
INSERT INTO Sluzby_na_Pokoji VALUES (2, 101, 1, 200);
INSERT INTO Sluzby_na_Pokoji VALUES (3, 101, 2, 150);
INSERT INTO Sluzby_na_Pokoji VALUES (4, 102, 2, 100);

INSERT INTO Zakaznik VALUES (12345678, 'User', 'Uzivatel', TO_DATE('1.5.1988','DD-MM-YYYY'), 'Kotahelky 13, 54321');
INSERT INTO Zakaznik VALUES (12345679, 'Hotel', 'Manager', TO_DATE('4.1.1990','DD-MM-YYYY'), 'Kotahelky 14, 54321');
INSERT INTO ZAKAZNIK VALUES (12345680, 'Admin', 'Administrator', TO_DATE('17.11.1987','DD-MM-YYYY'), 'Kotahelky 15, 54321');

INSERT INTO Rezervace VALUES (1, TO_DATE('14-03-2011','DD-MM-YYYY'), TO_DATE('15-03-2011','DD-MM-YYYY'), TO_DATE('20-03-2011','DD-MM-YYYY'), 1, 12345678);
INSERT INTO REZERVACE VALUES (2, TO_DATE('14-03-2012','DD-MM-YYYY'), TO_DATE('15-03-2012','DD-MM-YYYY'), TO_DATE('20-03-2012','DD-MM-YYYY'), 0, 12345678);

INSERT INTO Pobyt VALUES (1, TO_DATE('18-12-2012','DD-MM-YYYY'), TO_DATE('20-12-2012','DD-MM-YYYY'), 101, 1);
INSERT INTO Pobyt VALUES (2, TO_DATE('12-12-2012','DD-MM-YYYY'), TO_DATE('16-12-2012','DD-MM-YYYY'), 235, 2);
INSERT INTO Pobyt VALUES (3, TO_DATE('20-12-2012','DD-MM-YYYY'), TO_DATE('25-12-2012','DD-MM-YYYY'), 235, 2);

INSERT INTO Sluzby_v_Pobytu VALUES (1, 1, 1, 250);
INSERT INTO Sluzby_v_Pobytu VALUES (2, 1, 2, 350);
INSERT INTO Sluzby_v_Pobytu VALUES (3, 3, 1, 450);

INSERT INTO groups VALUES (1,'USER','Regular users');
INSERT INTO groups VALUES (2,'MANAGER','Manage all');
INSERT INTO groups VALUES (3,'ADMIN','Admin managers');

INSERT  INTO users VALUES (1,'user', 'ee11cbb19052e40b07aac0ca060c23ee', 12345678, 1);
INSERT  INTO users VALUES (2,'hotel', 'e919c49d5f0cd737285367810a3394d0', 12345679, 2);
INSERT  INTO users VALUES (3,'admin', '21232f297a57a5a743894a0e4a801fc3', 12345680, 3);
   
INSERT INTO usergroups(user_id,group_id) VALUES (1,1);
INSERT INTO usergroups(user_id,group_id) VALUES (2,2);
INSERT INTO usergroups(user_id,group_id) VALUES (3,3);

COMMIT;