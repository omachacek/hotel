package entity;

import entity.Pokoj;
import entity.Rezervace;
import entity.Zakaznik;
import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.2.v20111125-r10461", date="2012-11-29T00:22:41")
@StaticMetamodel(PobytyZakaznika.class)
public class PobytyZakaznika_ { 

    public static volatile SingularAttribute<PobytyZakaznika, Date> datumZacatku;
    public static volatile SingularAttribute<PobytyZakaznika, Pokoj> cisloPokoje;
    public static volatile SingularAttribute<PobytyZakaznika, BigInteger> idPobytu;
    public static volatile SingularAttribute<PobytyZakaznika, Rezervace> rezervace;
    public static volatile SingularAttribute<PobytyZakaznika, Date> datumKonce;
    public static volatile SingularAttribute<PobytyZakaznika, BigInteger> idRezervace;
    public static volatile SingularAttribute<PobytyZakaznika, BigInteger> cisloDokladu;
    public static volatile SingularAttribute<PobytyZakaznika, Zakaznik> zakaznik;

}