package entity;

import entity.Usergroups;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.2.v20111125-r10461", date="2012-11-29T00:22:41")
@StaticMetamodel(Groups.class)
public class Groups_ { 

    public static volatile SingularAttribute<Groups, BigDecimal> groupId;
    public static volatile SingularAttribute<Groups, String> groupName;
    public static volatile SingularAttribute<Groups, String> groupDesc;
    public static volatile CollectionAttribute<Groups, Usergroups> usergroupsCollection;

}