package entity;

import entity.PobytyZakaznika;
import entity.Rezervace;
import entity.Users;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.2.v20111125-r10461", date="2012-11-29T00:22:41")
@StaticMetamodel(Zakaznik.class)
public class Zakaznik_ { 

    public static volatile CollectionAttribute<Zakaznik, PobytyZakaznika> pobytyZakaznikaCollection;
    public static volatile SingularAttribute<Zakaznik, String> prijmeni;
    public static volatile SingularAttribute<Zakaznik, String> adresa;
    public static volatile SingularAttribute<Zakaznik, Users> usersCollection;
    public static volatile CollectionAttribute<Zakaznik, Rezervace> rezervaceCollection;
    public static volatile SingularAttribute<Zakaznik, BigDecimal> cisloDokladu;
    public static volatile SingularAttribute<Zakaznik, BigInteger> jeOp;
    public static volatile SingularAttribute<Zakaznik, String> jmeno;
    public static volatile SingularAttribute<Zakaznik, Date> datumNarozeni;

}