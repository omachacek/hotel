package entity;

import entity.Groups;
import entity.UsergroupsPK;
import entity.Users;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.2.v20111125-r10461", date="2012-11-29T00:22:41")
@StaticMetamodel(Usergroups.class)
public class Usergroups_ { 

    public static volatile SingularAttribute<Usergroups, UsergroupsPK> usergroupsPK;
    public static volatile SingularAttribute<Usergroups, Users> fkUsers;
    public static volatile SingularAttribute<Usergroups, Groups> fkGroups;

}