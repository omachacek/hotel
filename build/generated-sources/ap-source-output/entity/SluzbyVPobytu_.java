package entity;

import entity.Pobyt;
import entity.Sluzba;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.2.v20111125-r10461", date="2012-11-29T00:22:41")
@StaticMetamodel(SluzbyVPobytu.class)
public class SluzbyVPobytu_ { 

    public static volatile SingularAttribute<SluzbyVPobytu, Pobyt> idPobytu;
    public static volatile SingularAttribute<SluzbyVPobytu, BigInteger> sjednanaCenaSluzby;
    public static volatile SingularAttribute<SluzbyVPobytu, Sluzba> idSluzby;
    public static volatile SingularAttribute<SluzbyVPobytu, BigDecimal> sluzbyVPobytuId;

}