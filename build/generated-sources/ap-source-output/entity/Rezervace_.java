package entity;

import entity.Pobyt;
import entity.PobytyZakaznika;
import entity.Zakaznik;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.2.v20111125-r10461", date="2012-11-29T00:22:41")
@StaticMetamodel(Rezervace.class)
public class Rezervace_ { 

    public static volatile CollectionAttribute<Rezervace, PobytyZakaznika> pobytyZakaznikaCollection;
    public static volatile SingularAttribute<Rezervace, Date> datumVytvoreni;
    public static volatile CollectionAttribute<Rezervace, Pobyt> pobytCollection;
    public static volatile SingularAttribute<Rezervace, Date> datumPotvrzeni;
    public static volatile SingularAttribute<Rezervace, BigDecimal> idRezervace;
    public static volatile SingularAttribute<Rezervace, BigInteger> jeVHistorii;
    public static volatile SingularAttribute<Rezervace, Zakaznik> cisloDokladu;
    public static volatile SingularAttribute<Rezervace, Date> datumZaplaceni;

}