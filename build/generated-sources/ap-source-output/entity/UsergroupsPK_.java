package entity;

import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.2.v20111125-r10461", date="2012-11-29T00:22:41")
@StaticMetamodel(UsergroupsPK.class)
public class UsergroupsPK_ { 

    public static volatile SingularAttribute<UsergroupsPK, BigInteger> groupId;
    public static volatile SingularAttribute<UsergroupsPK, BigInteger> userId;

}