package entity;

import entity.SluzbyNaPokoji;
import entity.SluzbyVPobytu;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.2.v20111125-r10461", date="2012-11-29T00:22:41")
@StaticMetamodel(Sluzba.class)
public class Sluzba_ { 

    public static volatile SingularAttribute<Sluzba, String> popis;
    public static volatile SingularAttribute<Sluzba, BigDecimal> idSluzby;
    public static volatile SingularAttribute<Sluzba, String> nazev;
    public static volatile CollectionAttribute<Sluzba, SluzbyNaPokoji> sluzbyNaPokojiCollection;
    public static volatile CollectionAttribute<Sluzba, SluzbyVPobytu> sluzbyVPobytuCollection;

}