package entity;

import entity.Usergroups;
import entity.Zakaznik;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.2.v20111125-r10461", date="2012-11-29T00:22:41")
@StaticMetamodel(Users.class)
public class Users_ { 

    public static volatile SingularAttribute<Users, String> username;
    public static volatile SingularAttribute<Users, BigDecimal> userId;
    public static volatile SingularAttribute<Users, Zakaznik> zakaznikid;
    public static volatile CollectionAttribute<Users, Usergroups> usergroupsCollection;
    public static volatile SingularAttribute<Users, String> password;

}