package entity;

import entity.Pokoj;
import entity.Rezervace;
import entity.SluzbyVPobytu;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.2.v20111125-r10461", date="2012-11-29T00:22:41")
@StaticMetamodel(Pobyt.class)
public class Pobyt_ { 

    public static volatile SingularAttribute<Pobyt, Date> datumZacatku;
    public static volatile SingularAttribute<Pobyt, Pokoj> cisloPokoje;
    public static volatile SingularAttribute<Pobyt, BigDecimal> idPobytu;
    public static volatile SingularAttribute<Pobyt, Date> datumKonce;
    public static volatile SingularAttribute<Pobyt, Rezervace> idRezervace;
    public static volatile CollectionAttribute<Pobyt, SluzbyVPobytu> sluzbyVPobytuCollection;

}