package entity;

import entity.Pokoj;
import entity.Sluzba;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.2.v20111125-r10461", date="2012-11-29T00:22:41")
@StaticMetamodel(SluzbyNaPokoji.class)
public class SluzbyNaPokoji_ { 

    public static volatile SingularAttribute<SluzbyNaPokoji, BigInteger> cenaSluzby;
    public static volatile SingularAttribute<SluzbyNaPokoji, Pokoj> cisloPokoje;
    public static volatile SingularAttribute<SluzbyNaPokoji, BigDecimal> sluzbyNaPokojiId;
    public static volatile SingularAttribute<SluzbyNaPokoji, Sluzba> idSluzby;

}