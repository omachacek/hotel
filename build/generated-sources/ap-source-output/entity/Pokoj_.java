package entity;

import entity.Pobyt;
import entity.PobytyZakaznika;
import entity.SluzbyNaPokoji;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.2.v20111125-r10461", date="2012-11-29T00:22:41")
@StaticMetamodel(Pokoj.class)
public class Pokoj_ { 

    public static volatile CollectionAttribute<Pokoj, PobytyZakaznika> pobytyZakaznikaCollection;
    public static volatile SingularAttribute<Pokoj, BigDecimal> cisloPokoje;
    public static volatile SingularAttribute<Pokoj, BigInteger> pocetLuzek;
    public static volatile CollectionAttribute<Pokoj, Pobyt> pobytCollection;
    public static volatile CollectionAttribute<Pokoj, SluzbyNaPokoji> sluzbyNaPokojiCollection;
    public static volatile SingularAttribute<Pokoj, BigInteger> jeObsazen;
    public static volatile SingularAttribute<Pokoj, BigInteger> cena;

}