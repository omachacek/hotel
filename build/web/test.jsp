<%-- 
    Document   : test
    Created on : Oct 10, 2012, 9:30:50 PM
    Author     : ondra
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <sql:query var="result" dataSource="jdbc/hotel">
            SELECT * FROM pokoj
        </sql:query>

        <c:forEach var="category" items="${result.rows}">
            <c:out value="${category.cena}"/>
            <br />
        </c:forEach>

    </body>
</html>
